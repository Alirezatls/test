FROM openjdk:8-jdk-alpine
ADD build/libs/test.jar test.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]